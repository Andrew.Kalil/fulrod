﻿namespace fulrod_api.Config
{
    public class MongoDbSettings
    {
        //public string Host { get; set; }
        //public int Port { get; set; }
        //public string User { get; set; }
        //public string Password { get; set; }
        public string ConnectionString { get; set; } 
        public string DatabaseName { get; set; }
        public string[] CollectionNames { get; set; } 
        //{ 
        //    get
        //    {
        //        return $"mongodb://{User}:{Password}@{Host}:{Port}";
        //    } 
        //}
    }
}
