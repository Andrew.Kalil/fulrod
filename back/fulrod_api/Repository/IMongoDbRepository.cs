﻿using MongoDB.Driver;

namespace fulrod_api.Repository
{
    public interface IMongoDbRepository<T>
    {
        Task<IEnumerable<T>> GetAll(FilterDefinition<T> query = null, SortDefinition<T> sort = null);
        Task<T> GetBy(string id, FilterDefinition<T> query = null);
        Task Create(T model);
        Task Update(string id, T model);
        Task Delete(string id);
    }
}
