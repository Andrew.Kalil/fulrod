﻿using fulrod_api.Models;

namespace fulrod_api.Repository.Products
{
    public interface IProductRepository: IMongoDbRepository<Product>
    {
    }
}
