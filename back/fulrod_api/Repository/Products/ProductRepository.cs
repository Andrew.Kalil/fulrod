﻿using fulrod_api.Models;
using MongoDB.Driver;

namespace fulrod_api.Repository.Products
{
    public class ProductRepository : IProductRepository
    {
        private readonly IMongoCollection<Product> _products;
        public ProductRepository(MongoConnection connection)
        {
            _products = connection.GetCollection<Product>("Products");
        }

        public async Task Create(Product model)
        {
            await _products.InsertOneAsync(model);
        }

        public async Task Delete(string id)
        {
            await _products.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Product>> GetAll(FilterDefinition<Product> query, SortDefinition<Product> sort)
        {
            return await _products.Find(query).Sort(sort).ToListAsync();
        }

        public async Task<Product> GetBy(string id, FilterDefinition<Product> query = null)
        {
            return await _products.Find(product => product.Id == id).FirstOrDefaultAsync();
        }

        public async Task Update(string id, Product model)
        {
            await _products.ReplaceOneAsync(x => x.Id == id, model);
        }
    }
}
