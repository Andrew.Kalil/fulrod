﻿using fulrod_api.Models;
using MongoDB.Driver;
using System.Linq.Expressions;

namespace fulrod_api.Repository.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> _users;
        public UserRepository(MongoConnection connection)
        {
            _users = connection.GetCollection<User>("Users");
        }

        public async Task Create(User model)
        {
            await _users.InsertOneAsync(model);
        }

        public async Task Delete(string id)
        {
            await _users.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<User>> GetAll(FilterDefinition<User> query = null, SortDefinition<User> sort = null)
        {
            return await _users.Find(query).Sort(sort).ToListAsync();
        }

        public async Task<User> GetBy(string id, FilterDefinition<User> query = null)
        {
            return await _users.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<User> Login(string username)
        {
            return await _users.Find(x => x.Username == username).FirstOrDefaultAsync();
        }

        public async Task Update(string id, User model)
        {
            await _users.ReplaceOneAsync(x => x.Id == id, model);
        }
    }
}
