﻿using fulrod_api.Models;

namespace fulrod_api.Repository.Users
{
    public interface IUserRepository: IMongoDbRepository<User>
    {
        Task<User> Login(string username);
    }
}
