﻿using MongoDB.Driver;

namespace fulrod_api.Repository
{
    public class MongoConnection
    {
        private readonly IConfiguration _config;
        private readonly string _databaseName;
        private IMongoDatabase _database;
        public MongoConnection(IMongoClient mongoClient, IConfiguration config)
        {
            _config = config;
            _databaseName = _config.GetSection("MongoDbSettings:DatabaseName").Value;
            _database = mongoClient.GetDatabase(_databaseName);

        }
        public IMongoCollection<T> GetCollection<T>(string collection)
        {
            return this._database.GetCollection<T>(collection);
        }
    }
}
