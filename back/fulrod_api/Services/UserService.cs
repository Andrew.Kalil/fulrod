﻿using fulrod_api.Config;
using fulrod_api.Models;
using fulrod_api.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using fulrod_api.Repository.Users;
using System.Linq.Expressions;
using fulrod_api.Helprers.Interfaces;

namespace fulrod_api.Services
{
    public class UserService: IUserService
    {
        IUserRepository _repository;
        private readonly IConfiguration _configuration;
        IUserQueries _userQueries;

        public UserService(IUserRepository repository, IConfiguration config, IUserQueries userQueries)
        {
            _repository = repository;
            _configuration = config;
            _userQueries = userQueries;
        }

        public async Task<IEnumerable<User>> GetAsync(QueryTypes queries)
        {
            var newFilter = _userQueries.FilterByProperty(queries);
            var sort = _userQueries.Sort(queries);
            return await _repository.GetAll(newFilter, sort);
        }

        public async Task<User?> GetAsync(string id) =>
            await _repository.GetBy(id);

        public string GetConnectionString()
        {
            var configuration = _configuration.GetSection(nameof(MongoDbSettings)).Get<MongoDbSettings>();
            return configuration.ConnectionString;
        }
        public async Task<User> CreateAsync(UserCreate userCreate)
        {
            var users = await _repository.GetAll();
            CreatePasswordHash(userCreate.Password, out byte[] passwordHash, out byte[] passwordSalt);
            var user = new User();
            user.Username = userCreate.Username;
            user.EmailAddress = userCreate.EmailAddress;
            user.GivenName = userCreate.GivenName;
            user.Surname = userCreate.Surname;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.Role = users.IsNullOrEmpty() ? "Admin" : "Default";
            await _repository.Create(user);
            return user;
        }
        public async Task<LoginResponse> LoginAsync(UserLogin userLogin)
        {
            var user = await _repository.Login(userLogin.Username);
            var loginResponse = new LoginResponse();
            if (user == null)
            {
                loginResponse.Response = "Fail";
                loginResponse.Token = null;
                return loginResponse;
            }
            if (!VerifyPasswordHash(userLogin.Password, user.PasswordHash, user.PasswordSalt))
            {
                loginResponse.Response = "Fail";
                loginResponse.Token = null;
                return loginResponse;
            }
            string token = CreateToken(user);
            var claims = new JwtSecurityToken(token).Claims.ToList();
            loginResponse.Response = "Success";
            loginResponse.Token = token;
            loginResponse.UserId = claims[0].Value;
            loginResponse.Role = claims[2].Value;
            return loginResponse;
        }
        public async Task UpdateAsync(string id, UserUpdate updatedUser)
        {
            var user = await _repository.GetBy(id);
            if (updatedUser.Password != null && !VerifyPasswordHash(updatedUser.Password, user.PasswordHash, user.PasswordSalt))
            {
                CreatePasswordHash(updatedUser.Password, out byte[] passwordHash, out byte[] passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }
            user.Username = updatedUser.Username;
            user.EmailAddress = updatedUser.EmailAddress;
            user.GivenName = updatedUser.GivenName;
            user.Surname = updatedUser.Surname;
            user.Role = updatedUser.Role;
            await _repository.Update(id, user);
        }


        public async Task RemoveAsync(string id) =>
            await _repository.Delete(id);

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computeHash.SequenceEqual(passwordHash);
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role),
                new Claim(ClaimTypes.Email, user.EmailAddress)
            };
            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: cred
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
    }
}
