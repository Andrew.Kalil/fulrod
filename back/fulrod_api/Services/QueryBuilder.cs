﻿using fulrod_api.Models;
using fulrod_api.Services.Interfaces;
using System.Text.Json;

namespace fulrod_api.Services
{
    public class QueryBuilder : IQueryBuilder
    {
        public Query parseQuery(string query)
        {
            Query filterQuery = new Query();
            if (!string.IsNullOrEmpty(query))
            {
                string[] parsedString = query.Split("=");
                if (parsedString.Length <= 2)
                {
                    filterQuery.Field = parsedString[0];
                    filterQuery.Value = parsedString[1];
                } else if (parsedString.Length > 2 && parsedString.Length <= 3)
                {
                    filterQuery.Field = parsedString[0];
                    filterQuery.Value = parsedString[1];
                    filterQuery.Option = parsedString[2];
                } 
            }
            return filterQuery;
        }
    }
}
