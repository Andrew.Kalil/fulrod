﻿using fulrod_api.Models;

namespace fulrod_api.Services.Interfaces
{
    public interface IQueryBuilder
    {
        Query parseQuery(string query);
    }
}
