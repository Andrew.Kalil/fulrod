﻿using fulrod_api.Models;

namespace fulrod_api.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAsync(QueryTypes queries);
        Task<User?> GetAsync(string id);
        Task<User> CreateAsync(UserCreate userCreate);
        Task<LoginResponse> LoginAsync(UserLogin userLogin);
        Task UpdateAsync(string id, UserUpdate updatedUser);
        Task RemoveAsync(string id);
        string GetConnectionString();
    }
}
