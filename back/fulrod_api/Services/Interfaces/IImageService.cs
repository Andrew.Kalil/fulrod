﻿using CloudinaryDotNet.Actions;

namespace fulrod_api.Services.Interfaces
{
    public interface IImageService
    {
        Task<ImageUploadResult> UploadImageAsync(IFormFile image);
        Task<DeletionResult> DeleteImageAsync(string publicId);
    }
}
