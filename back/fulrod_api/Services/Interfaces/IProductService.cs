﻿using fulrod_api.Models;

namespace fulrod_api.Services.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAsync(QueryTypes queries);
        Task<Product?> GetAsync(string id);
        Task CreateAsync(Product newProduct);
        Task UpdateAsync(string id, Product updatedProduct);
        Task RemoveAsync(string id);
        Task<ImageModel> UploadImageAsync(IFormFile image, string id);

    }
}
