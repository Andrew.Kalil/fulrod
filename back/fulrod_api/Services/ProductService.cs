﻿using fulrod_api.Helprers;
using fulrod_api.Helprers.Interfaces;
using fulrod_api.Models;
using fulrod_api.Repository.Products;
using fulrod_api.Services.Interfaces;
using System.Text.Json;

namespace fulrod_api.Services
{
    public class ProductService: IProductService
    {
        private readonly IImageService _imageService;
        IProductRepository _productRepository;
        IProductQueries _productQueries;

        public ProductService(IProductRepository productRepository, IImageService imageService, IProductQueries productFilters)
        {
            _imageService = imageService;
            _productRepository = productRepository;
            _productQueries = productFilters;
        }

        public async Task<IEnumerable<Product>> GetAsync(QueryTypes queries)
        {
            var newFilter = _productQueries.FilterByProperty(queries);
            var sort = _productQueries.Sort(queries);
            return await _productRepository.GetAll(newFilter, sort);
        }

        public async Task<Product?> GetAsync(string id) =>
            await _productRepository.GetBy(id);

        public async Task CreateAsync(Product newProduct) =>
            await _productRepository.Create(newProduct);


        public async Task UpdateAsync(string id, Product updatedProduct) =>
            await _productRepository.Update(id, updatedProduct);

        public async Task RemoveAsync(string id)
        {
            var product = await this.GetAsync(id);
            if (product.Image.ImageUrl != null)
            {
                _ = await _imageService.DeleteImageAsync(product.Image.PublicId);
            }
            await _productRepository.Delete(id);
        }

        public async Task<ImageModel> UploadImageAsync(IFormFile image, string id)
        {
            var product = await this.GetAsync(id);

            if (product.Image != null && product.Image.ImageUrl != null)
            {
                var deleteResults = await _imageService.DeleteImageAsync(product.Image.PublicId);
            }
            var result = await _imageService.UploadImageAsync(image);
            var photo = new ImageModel
            {
                ImageUrl = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId,
                IsPrimary = true
            };
            product.Image = photo;
            await _productRepository.Update(id, product);
            return photo;
        }
    }
}
