﻿using Microsoft.AspNetCore.Mvc;

namespace fulrod_api.Models
{
    public class QueryTypes
    {
        public string property { get; set; }
        public string sort { get; set; }
    }
}
