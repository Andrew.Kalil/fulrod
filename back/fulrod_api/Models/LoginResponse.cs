namespace fulrod_api.Models
{
    public class LoginResponse
    {
        public string Response { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public string Role { get; set; }    
    }
}