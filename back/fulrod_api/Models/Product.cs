﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace fulrod_api.Models
{
    public class Product
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; } = null!;    
        public string Description { get; set; } = null!;
        public ImageModel Image { get; set; } = null!;
        public int Category { get; set; }
        public int Price { get; set; }  
        public bool Promotion { get; set; } 
        public decimal Discount { get; set; }
        public int Quantity { get; set; }
    }
}
