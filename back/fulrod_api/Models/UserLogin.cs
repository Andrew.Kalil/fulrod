﻿namespace fulrod_api.Models
{
    public class UserLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }    
    }
}
