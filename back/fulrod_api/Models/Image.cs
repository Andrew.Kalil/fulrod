using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace fulrod_api.Models
{
    public class ImageModel
    {
        public string PublicId { get; set; }
        public string ImageUrl { get; set; }
        
        public bool IsPrimary { get; set; }
    }
}