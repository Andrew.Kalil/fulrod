using fulrod_api.Config;
using fulrod_api.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Filters;
using System.Text;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using System.Text.Json;
using System.Net.Mime;
using System.Security.Authentication;
using fulrod_api.Services.Interfaces;
using fulrod_api.Repository;
using fulrod_api.Repository.Users;
using fulrod_api.Repository.Products;
using fulrod_api.Helprers;
using fulrod_api.Helprers.Interfaces;
using fulrod_api.Helprers.Queries;
using Api.Core;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//builder.Services.Configure<DbSettings>(
//    builder.Configuration.GetSection("FulrodDatabase"));

builder.Services.AddSingleton<MongoConnection>();
builder.Services.AddSingleton<IUserRepository, UserRepository>();
builder.Services.AddSingleton<IProductRepository, ProductRepository>();

builder.Services.AddSingleton<IProductService, ProductService>();
builder.Services.AddSingleton<IUserService, UserService>();
builder.Services.AddSingleton<IImageService, ImageService>();

// Helpers
builder.Services.AddSingleton<IProductQueries, ProductQueries>();
builder.Services.AddSingleton<IUserQueries, UserQueries>();
builder.Services.AddSingleton<IQueryBuilder, QueryBuilder>();
builder.Services.AddSingleton<IUtils, Utils>();

//builder.Services.AddMemoryCache();

var settings = builder.Configuration.GetSection(nameof(MongoDbSettings)).Get<MongoDbSettings>();
string _myAllowSpecificOrigins = "_myAllowSpecificOrigins";

builder.Services.AddDbContext<ApiDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddSingleton<IMongoClient>(serviceProvider =>
{
    MongoClientSettings mongoConfig = MongoClientSettings.FromUrl(
        new MongoUrl(settings.ConnectionString)
    );
    mongoConfig.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
    var mongoClient = new MongoClient(mongoConfig);
    return mongoClient;
});

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: _myAllowSpecificOrigins,
                      builder =>
                      {
                          builder
                            .SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost")
                            .AllowAnyHeader()
                            .AllowAnyMethod();

                      });
});


builder.Services.AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme()
    {
        Description = "Standard Autherization header using the bearer scheme (\"bearer {token}\")",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });
    options.OperationFilter<SecurityRequirementsOperationFilter>();
});
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("AppSettings:Token").Value)),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });
builder.Services.AddHealthChecks()
    .AddMongoDb(
        settings.ConnectionString,
        name: "mongodb",
        timeout: TimeSpan.FromSeconds(3),
        tags: new[] {"ready"}
    );

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseHttpsRedirection();
}

app.UseCors(_myAllowSpecificOrigins);

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();;

app.MapHealthChecks("/api/health/ready", new HealthCheckOptions {
    Predicate = (check) => check.Tags.Contains("ready"),
    ResponseWriter = async(context, report) =>
    {
        var result = JsonSerializer.Serialize(
            new
            {
                status = report.Status.ToString(),
                checks = report.Entries.Select(entry => new
                {
                    name = entry.Key,
                    status = entry.Value.Status.ToString(),
                    exception = entry.Value.Exception != null ? 
                                    entry.Value.Exception.Message : 
                                    "None",
                    duration = entry.Value.Duration.ToString(),
                })
            }
        );
        context.Response.ContentType = MediaTypeNames.Application.Json;
        await context.Response.WriteAsync(result);
    }
});
app.MapHealthChecks("/api/health/live", new HealthCheckOptions
{
    Predicate = (_) => false
});

app.Run();
