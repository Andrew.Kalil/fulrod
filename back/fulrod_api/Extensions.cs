﻿using fulrod_api.Models;

namespace fulrod_api
{
    public static class Extensions
    {
        // For sending back user entity data
        public static UserDto AsUserDto(this User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Username = user.Username,   
                EmailAddress = user.EmailAddress, 
                GivenName = user.GivenName,
                Surname = user.Surname,
                Role = user.Role,
            };
        }
    }
}
