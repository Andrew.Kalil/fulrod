﻿using fulrod_api.Models;
using fulrod_api.Services;
using fulrod_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fulrod_api.Controllers;


[ApiController]
[Route("api/[controller]")]
public class ProductsController : ControllerBase
{
    private readonly IProductService _productService;
    private readonly ILogger<ProductsController> _logger;

    public ProductsController(ProductService productService, ILogger<ProductsController> logger)
    {
        _productService = productService;
        _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]QueryTypes queries)
    {
        var products = await _productService.GetAsync(queries);
        _logger.LogInformation($"{DateTime.UtcNow.ToString("hh:mm:ss")}: Retrieved {products.Count()} products");
        return Ok(products);
    }

    [HttpGet("{id:length(24)}")]
    public async Task<IActionResult> Get(string id)
    {
        var product = await _productService.GetAsync(id);

        // unecessary in this cape
        if (product is null)
        {
            return NotFound();
        }

        return Ok(product);
    }
    
    [Authorize(Roles = "Admin")]
    [HttpPost]
    public async Task<IActionResult> Post(Product newProduct)
    {
        await _productService.CreateAsync(newProduct);
        var id = new { id = newProduct.Id };
        
        return CreatedAtAction(nameof(Get), id, newProduct);
    }

    [Authorize(Roles = "Admin")]
    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Product updatedProduct)
    {
        var product = await _productService.GetAsync(id);

        if (product is null)
        {
            return NotFound();
        }

        updatedProduct.Id = product.Id;

        // more specific with name
        await _productService.UpdateAsync(id, updatedProduct);

        return NoContent();
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var product = await _productService.GetAsync(id);

        if (product is null)
        {
            return NotFound();
        }

        await _productService.RemoveAsync(product.Id);

        return NoContent();
    }

    [Authorize(Roles = "Admin")]
    [HttpPost("image/{id}")]
    public async Task<ActionResult<ImageModel>> UploadImage(IFormFile image, string id)
    {
        var imageResult = await _productService.UploadImageAsync(image, id);

        return Ok(imageResult);
    }
}
