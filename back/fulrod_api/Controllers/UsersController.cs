﻿using fulrod_api.Models;
using fulrod_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace fulrod_api.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(Roles = "Admin")]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;

    public UsersController(IUserService userService) =>
        _userService = userService;

    [HttpGet]
    public async Task<IEnumerable<UserDto>> Get([FromQuery] QueryTypes queries)
    {
        var userList = await _userService.GetAsync(queries);
        return userList.Select(item => item.AsUserDto());
    }

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<UserDto>> Get(string id)
    {
        var user = await _userService.GetAsync(id);

        if (user is null)
        {
            return NotFound();
        }

        return user.AsUserDto();
    }

    [HttpGet("connectionString")]
    public ActionResult<string> GetConnectionString(){
        var connectionString = _userService.GetConnectionString();
        return Ok(connectionString);
    }

    [HttpPost("register")]
    [AllowAnonymous]
    public async Task<IActionResult> Post(UserCreate userCreate)
    {
        await _userService.CreateAsync(userCreate);

        //return CreatedAtAction(nameof(Get), new { id = newProduct.Id }, newProduct);
        return Ok();
    }

    [HttpPost("login")]
    [AllowAnonymous]
    public async Task<ActionResult> Login(UserLogin userLogin)
    {
        var loginResponse = await _userService.LoginAsync(userLogin);
        return Ok(loginResponse);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, UserUpdate updatedUser)
    {
        var user = await _userService.GetAsync(id);

        if (user is null)
        {
            return NotFound();
        }

        updatedUser.Id = user.Id;

        await _userService.UpdateAsync(id, updatedUser);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var user = await _userService.GetAsync(id);

        if (user is null)
        {
            return NotFound();
        }

        await _userService.RemoveAsync(user.Id);

        return NoContent();
    }
}

