﻿using fulrod_api.Helprers.Interfaces;

namespace fulrod_api.Helprers
{
    public class Utils : IUtils
    {
        public string Catipalize(string targetString)
        {
            if (targetString.Length == 0)
                return string.Empty;
            else if (targetString.Length == 1)
                return char.ToUpper(targetString[0]).ToString();
            else
                return $"{char.ToUpper(targetString[0]).ToString()}{targetString.Substring(1)}";
        }
    }
}
