﻿using fulrod_api.Models;
using MongoDB.Driver;

namespace fulrod_api.Helprers.Interfaces
{
    public interface IFilters<T>
    {
        FilterDefinition<T> FilterByProperty(QueryTypes queryType);
        SortDefinition<T> Sort(QueryTypes queryType);
    }
}
