﻿using fulrod_api.Helprers.Interfaces;
using fulrod_api.Models;
using fulrod_api.Services.Interfaces;
using MongoDB.Driver;

namespace fulrod_api.Helprers.Queries
{
    public class UserQueries : IUserQueries
    {
        private IQueryBuilder _queryBuilder;
        private IUtils _utils;
        public UserQueries(IQueryBuilder queryBuilder, IUtils utils)
        {
            _queryBuilder = queryBuilder;
            _utils = utils;
        }
        public FilterDefinition<User> FilterByProperty(QueryTypes queries)
        {
            FilterDefinition<User> result = FilterDefinition<User>.Empty;
            Query query;
            if (queries.property != null || !string.IsNullOrEmpty(queries.property))
            {
                query = _queryBuilder.parseQuery(queries.property);
                if (string.IsNullOrEmpty(query.Option))
                {
                    switch (query.Option)
                    {
                        case "lt":
                            result = Builders<User>.Filter.Lt(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "lte":
                            result = Builders<User>.Filter.Lte(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "gt":
                            result = Builders<User>.Filter.Gt(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "gte":
                            result = Builders<User>.Filter.Gte(_utils.Catipalize(query.Field), query.Value);
                            break;
                        default:
                            result = Builders<User>.Filter.Eq(_utils.Catipalize(query.Field), query.Value);
                            break;
                    }
                }
                else if (query.Field != null && query.Value != null)
                {
                    result = Builders<User>.Filter.Eq(_utils.Catipalize(query.Field), query.Value);
                }
            }
            return result;
        }

        public SortDefinition<User> Sort(QueryTypes queries)
        {
            Query query;
            if (queries.sort != null || string.IsNullOrEmpty(queries.sort))
            {
                query = _queryBuilder.parseQuery(queries.sort);
                if (query.Field != null && query.Value != null)
                {
                    if ((string)query.Value == "ASC")
                    {
                        return Builders<User>.Sort.Ascending(_utils.Catipalize(query.Field));
                    }
                    else if ((string)query.Value == "DSC")
                    {
                        return Builders<User>.Sort.Descending(_utils.Catipalize(query.Field));
                    }
                }
                return null;
            }
            return null;
        }
    }
}
