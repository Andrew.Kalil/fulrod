﻿using fulrod_api.Helprers.Interfaces;
using fulrod_api.Models;
using fulrod_api.Services.Interfaces;
using MongoDB.Driver;
using System.Text.Json;

namespace fulrod_api.Helprers
{
    public class ProductQueries : IProductQueries
    {
        private IQueryBuilder _queryBuilder;
        private IUtils _utils;
        public ProductQueries(IQueryBuilder queryBuilder, IUtils utils)
        {
            _queryBuilder = queryBuilder;
            _utils = utils;
        }

        public FilterDefinition<Product> FilterByProperty(QueryTypes queries)
        {
            FilterDefinition<Product> result = FilterDefinition<Product>.Empty;
            Query query;
            if (queries.property != null || !string.IsNullOrEmpty(queries.property))
            {
                query = _queryBuilder.parseQuery(queries.property);
                if (!string.IsNullOrEmpty(query.Option))
                {
                    Console.WriteLine(JsonSerializer.Serialize(query));
                    switch (query.Option)
                    {
                        case "lt":
                            result = Builders<Product>.Filter.Lt(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "lte":
                            result = Builders<Product>.Filter.Lte(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "gt":
                            result = Builders<Product>.Filter.Gt(_utils.Catipalize(query.Field), query.Value);
                            break;
                        case "gte":
                            result = Builders<Product>.Filter.Gte(_utils.Catipalize(query.Field), query.Value);
                            break;
                        default:
                            result = Builders<Product>.Filter.Eq(_utils.Catipalize(query.Field), query.Value);
                            break;
                    }
                }
                else if (query.Field != null && query.Value != null)
                {
                    result = Builders<Product>.Filter.Eq(_utils.Catipalize(query.Field), query.Value);
                }
            }
            return result;
        }

        public SortDefinition<Product> Sort(QueryTypes queries)
        {
            Query query;
            if (queries.sort != null || string.IsNullOrEmpty(queries.sort))
            {
                query = _queryBuilder.parseQuery(queries.sort);
                if (query.Field != null && query.Value != null)
                {
                    if ((string)query.Value == "ASC")
                    {
                        return Builders<Product>.Sort.Ascending(_utils.Catipalize(query.Field));
                    }
                    else if ((string)query.Value == "DSC")
                    {
                        return Builders<Product>.Sort.Descending(_utils.Catipalize(query.Field));
                    }
                }
                return null;
            }
            return null;
        }
    }
}
