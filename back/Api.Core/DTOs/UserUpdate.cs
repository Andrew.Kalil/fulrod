﻿using System.ComponentModel.DataAnnotations;

namespace Api.Core.Entities
{
    public class UserUpdate
    {        public string Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        [Required]
        public string GivenName { get; set; }
        [Required]
        public string Surname { get; set; }
        public string Role { get; set; }
    }
}
