﻿namespace Api.Core.Entities
{
    public class Query
    {
        public string Field { get; set; }
        public object Value { get; set; }
        public string Option { get; set; }
    }
}
