﻿using System.ComponentModel.DataAnnotations;

namespace Api.Core.Entities
{
    public class UserCreate
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string GivenName { get; set; }
        [Required]
        public string Surname { get; set; }
    }
}
