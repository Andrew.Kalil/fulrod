﻿namespace Api.Core.Entities
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
    }
}
