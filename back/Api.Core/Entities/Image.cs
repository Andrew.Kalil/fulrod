namespace Api.Core.Entities
{
    public class ImageModel
    {
        public string PublicId { get; set; }
        public string ImageUrl { get; set; }
        
        public bool IsPrimary { get; set; }
    }
}