﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Core.Entities
{
    [Table("User", Schema = "dbo")]
    public class Category
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }    
        public IEnumerable<Product> Products { get; set; }
    }
}
