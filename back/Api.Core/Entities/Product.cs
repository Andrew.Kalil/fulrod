﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Core.Entities
{
    [Table("Product", Schema = "dbo")]
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }
        public string Name { get; set; }  
        public string Description { get; set; }
        public ImageModel Image { get; set; }
        public int Price { get; set; }  
        public bool Promotion { get; set; } 
        public decimal Discount { get; set; }
        public int Quantity { get; set; }
        public virtual Guid CategoryId { get; set; }

        [ForeignKey(nameof(CategoryId))]
        public virtual Category Category { get; set; }
    }
}
