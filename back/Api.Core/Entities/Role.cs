﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Core.Entities
{
    [Table("Role", Schema = "dbo")]
    public class Role
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
