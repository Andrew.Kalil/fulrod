import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ProductsModule } from './pages/products/products.module';
import { UsersModule } from './pages/users/users.module';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './pages/home/home.module';
import { LoginModule } from './pages/login/login.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { productReducer } from './state/products/products.reducers';
import { ProductsEffects } from './state/products/products.effects';
import { ProductsService } from './core/services/products/products.service';
import { userReducer } from './state/users/users.reducers';
import { UsersEffects } from './state/users/users.effects';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    UsersModule,
    ProductsModule,
    HomeModule,
    LoginModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({ products: productReducer, users: userReducer }),
    EffectsModule.forRoot([ProductsEffects, UsersEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
