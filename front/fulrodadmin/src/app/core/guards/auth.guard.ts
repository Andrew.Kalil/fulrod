import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  isLogged: boolean = false;
  isAdmin: boolean = false;
  constructor(private router: Router, private auth: AuthService) {
    this.auth.isLogged.subscribe((value) => {
      this.isLogged = value;
    });
    this.auth.user.subscribe((user: User) => {
      this.isAdmin = user.role === 'Admin';
    });
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this.isLogged) {
      return true;
    } else {
      sessionStorage.setItem('redirectPage', state.url);
      this.router.navigate(['/home']);
      return false;
    }
  }
}
