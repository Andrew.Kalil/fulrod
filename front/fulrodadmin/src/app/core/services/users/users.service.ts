import { HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Queries } from '../../models/filter';
import { User } from '../../models/user.model';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  path = 'Users/';
  constructor(private apiService: ApiService) {}

  getBy(id: string): Observable<User> {
    return this.apiService.getBy(this.path, id);
  }

  getAll(queries?: Queries): Observable<User[]> {
    if (queries) {
      const params = new HttpParams()
        .set('property', queries.property ? queries.property : '')
        .set('sort', queries.sort ? queries.sort : '');

      return this.apiService.getAll(this.path, params);
    }
    return this.apiService.getAll(this.path);
  }

  create(user: User): Observable<any> {
    return this.apiService.post(`${this.path}register`, user);
  }

  update(user: User, id: string): Observable<any> {
    return this.apiService.put(this.path, id, user);
  }

  delete(id: string): Observable<any> {
    return this.apiService.delete(this.path, id);
  }
}
