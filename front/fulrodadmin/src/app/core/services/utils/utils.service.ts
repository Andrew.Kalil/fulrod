import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  addQuery(queryString: string, entry: string) {
    let list = queryString.split('&');
    const newList = list.map((query) => {
      if (
        query.split('=')[0] === 'category' &&
        entry.split('=')[0] === 'category'
      ) {
        return entry;
      } else {
        return query;
      }
    });
    if (entry === 'NONE') {
      return '';
    } else {
      return newList.join('&');
    }
  }
}
