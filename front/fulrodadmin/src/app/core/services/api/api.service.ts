import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(protected http: HttpClient) {}

  protected formatErrors(error: any) {
    return throwError(error.error);
  }

  baseUrl(path: string) {
    return `${environment.apiBaseURL}${path}`;
  }

  buildHeaders(headers: any) {
    // eslint-disable-next-line no-param-reassign
    headers.Accept = headers.Accept ? headers.Accept : 'application/json';
    headers['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
    // eslint-disable-next-line no-param-reassign
    headers['Content-Type'] = headers['Content-Type']
      ? headers['Content-Type']
      : 'application/json';
    return headers;
  }

  getBy(
    path: string,
    id: string,
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    const url = this.baseUrl(`${path}${id}`);
    return this.http
      .get(url, { params, headers: this.buildHeaders({}) })
      .pipe(catchError(this.formatErrors));
  }

  getAll(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    const url = this.baseUrl(path);
    return this.http
      .get(url, { params, headers: this.buildHeaders({}) })
      .pipe(catchError(this.formatErrors));
  }

  post(
    path: string,
    body: Object,
    params: HttpParams = new HttpParams(),
    headers: HttpHeaders | null = null
  ): Observable<any> {
    const url = this.baseUrl(path);
    return this.http
      .post(url, JSON.stringify(body), {
        params,
        headers: this.buildHeaders(headers || {}),
      })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, id: string, body: Object): Observable<any> {
    const url = this.baseUrl(`${path}${id}`);
    return this.http
      .put(url, JSON.stringify(body), { headers: this.buildHeaders({}) })
      .pipe(catchError(this.formatErrors));
  }

  patch(path: string, id: string, body: Object): Observable<any> {
    const url = this.baseUrl(`${path}${id}`);
    return this.http
      .patch(url, JSON.stringify(body), { headers: this.buildHeaders({}) })
      .pipe(catchError(this.formatErrors));
  }

  delete(path: string, id: string): Observable<any> {
    const url = this.baseUrl(`${path}${id}`);
    return this.http
      .delete(url, { headers: this.buildHeaders({}) })
      .pipe(catchError(this.formatErrors));
  }
}
