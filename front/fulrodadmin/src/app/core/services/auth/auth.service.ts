import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../../models/login.model';
import { LoginResponse } from '../../models/loginResponse.model';
import { User } from '../../models/user.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiService } from '../api/api.service';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public readonly ADMIN_ROLE: string = 'Admin';
  private loggedIn = new BehaviorSubject<boolean>(false);
  user = new BehaviorSubject<User>({} as User);

  api_url!: string;

  constructor(private http: HttpClient, private apiService: ApiService) {
    this.api_url = this.apiService.baseUrl('Users');
    this.checkToken();
  }

  get isLogged(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  login(data: Login): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.api_url}/login`, data).pipe(
      map((res: LoginResponse) => {
        this.saveToken(res.token, res.userId);
        this.loggedIn.next(true);
        return res;
      }),
      catchError((err: HttpErrorResponse) => this.handleError(err))
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    this.loggedIn.next(false);
  }

  private checkToken(): void {
    const userToken: string | null = localStorage.getItem('token');
    // const userId: string | null = localStorage.getItem('userId') || '';
    const isExpired = helper.isTokenExpired(userToken || '');
    if (isExpired) {
      this.logout();
    } else {
      this.loggedIn.next(true);
      // this.setUser(userId);
    }
  }

  private saveToken(token: string, userId: string): void {
    localStorage.setItem('token', token);
    // localStorage.setItem('userId', userId);
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    let errorMessage = 'An error ocurred retrieving data';
    if (err) {
      errorMessage = `Error: code ${err.message}`;
    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }

  setUser(userId: string): Observable<User> {
    return this.apiService.getBy('Users/', userId).pipe(
      map((user) => {
        this.user.next(user);
        return user;
      })
    );
  }
}
