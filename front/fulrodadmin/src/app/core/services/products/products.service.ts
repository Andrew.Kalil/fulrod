import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Queries } from '../../models/filter';
import { Image } from '../../models/image.model';
import { Product } from '../../models/product.model';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  path = 'Products/';
  constructor(private apiService: ApiService, private http: HttpClient) {}

  getBy(id: string): Observable<Product> {
    return this.apiService.getBy(this.path, id);
  }

  getAll(queries?: Queries): Observable<Product[]> {
    if (queries) {
      const params = new HttpParams()
        .set('property', queries.property ? queries.property : '')
        .set('sort', queries.sort ? queries.sort : '');

      return this.apiService.getAll(this.path, params);
    }
    return this.apiService.getAll(this.path);
  }

  create(product: Product, image?: File): Observable<any> {
    return this.apiService.post(this.path, product);
  }

  update(id: string, product: Product): Observable<any> {
    return this.apiService.put(this.path, id, product);
  }

  delete(id: string): Observable<any> {
    return this.apiService.delete(this.path, id);
  }

  createImage(id: string, body: FormData): Observable<Image> {
    let url = this.apiService.baseUrl(`${this.path}image/${id}`);

    return this.http.post<Image>(url, body, {
      headers: {
        ['authorization']: `Bearer ${window.localStorage.getItem('token')}`,
      },
    });
  }
}
