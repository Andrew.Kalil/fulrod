export interface Image {
  publicId?: string;
  imageUrl?: string;
  isPrimary?: boolean;
  file?: File;
}
