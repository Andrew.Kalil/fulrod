import { Image } from './image.model';

export interface Product {
  id?: string;
  name: string;
  description: string;
  image?: Image;
  category: number;
  price: number;
  promotion: boolean;
  discount: number;
  quantity: number;
}
