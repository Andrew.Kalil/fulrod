export interface Column {
  property: string;
  displayName: string;
}
