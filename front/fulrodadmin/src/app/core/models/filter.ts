export interface Queries {
  // {property}={value}
  property?: string;
  // {property}:{sortType}
  sort?: string;
}
