export interface User {
  id: string;
  username: string;
  emailAddress: string;
  givenName: string;
  surname: string;
  role: string;
}
