import { FilterOptions } from './filterOptions.model';

export interface OrderOptions extends FilterOptions {}
