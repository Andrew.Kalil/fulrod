export interface LoginResponse {
  response: string;
  token: string;
  userId: string;
  role: string;
}
