export interface FilterOptions {
  option: string;
  callback: () => void;
}
