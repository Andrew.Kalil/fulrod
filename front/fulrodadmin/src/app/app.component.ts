import { Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isLogged: boolean = false;
  constructor(private authService: AuthService) {
    this.authService.isLogged.subscribe((value: boolean) => {
      this.isLogged = value;
    });
  }
  title = 'fulrodadmin';
}
