import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { User } from 'src/app/core/models/user.model';
import { UserState } from 'src/app/state/users/users.reducers';
import { userSelector } from 'src/app/state/users/users.selectors';

export interface UserModalData {
  user?: User;
  isCreate: boolean;
  addUser?: (user: User) => {};
  updateUser?: (id: string, user: User) => {};
}

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss'],
})
export class UserModalComponent implements OnInit {
  userForm!: FormGroup;
  userSubscription: any;
  singleUser!: User;
  isLoading!: boolean;
  // newImage: BehaviorSubject<Image> = new BehaviorSubject<Image>({} as Image);
  constructor(
    public dialogRef: MatDialogRef<UserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserModalData,
    private userStore: Store<UserState>,
    private formBuilder: FormBuilder
  ) {
    this.userSubscription = this.userStore
      .select(userSelector)
      .subscribe((value) => {
        this.singleUser = value.currentUser;
        this.isLoading = value.loading;
        // !this.data.isCreate && this.setFormValues(value.currentUser);
      });
  }

  ngOnInit(): void {
    this.initForm();
    // this.productForm.valueChanges.subscribe((value) => console.log(value));
  }

  // Inital form values
  initForm() {
    this.userForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      emailAddress: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      givenName: new FormControl('', [Validators.required]),
      surname: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        this.matchValidator('confirmPassword', true),
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        this.matchValidator('password'),
      ]),
    });

    if (!this.data.isCreate) {
      this.setFormValues(this.singleUser);
    }
  }

  // Set form values if edit
  setFormValues(user: User) {
    if (user) {
      const { username, emailAddress, givenName, surname, role } = user;
      this.userForm.get('username')?.setValue(username);
      this.userForm.get('emailAddress')?.setValue(emailAddress);
      this.userForm.get('givenName')?.setValue(givenName);
      this.userForm.get('surname')?.setValue(surname);
      this.userForm.addControl(
        'role',
        new FormControl(role, [Validators.required])
      );
      this.userForm.removeControl('password');
      this.userForm.removeControl('confirmPassword');
    }
  }

  // Submit form function
  onSubmit(event: Event): void {
    const newUser: any = {
      username: this.userForm.get('username')?.value,
      givenName: this.userForm.get('givenName')?.value,
      surname: this.userForm.get('surname')?.value,
      role: this.userForm.get('role')?.value,
      emailAddress: this.userForm.get('emailAddress')?.value,
      password: this.userForm.get('confirmPassword')?.value,
    };
    const updateUser: any = {
      username: this.userForm.get('username')?.value,
      givenName: this.userForm.get('givenName')?.value,
      surname: this.userForm.get('surname')?.value,
      role: this.userForm.get('role')?.value,
      emailAddress: this.userForm.get('emailAddress')?.value,
    };
    event.preventDefault();
    if (this.data.isCreate && this.data.addUser) {
      this.data.addUser(newUser);
    } else if (!this.data.isCreate && this.data.updateUser) {
      this.data.updateUser(this.singleUser.id, updateUser);
    }
    this.dialogRef.close();
  }

  matchValidator(matchTo: string, reverse?: boolean): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.parent && reverse) {
        const c = (control.parent?.controls as any)[matchTo] as AbstractControl;
        if (c) {
          c.updateValueAndValidity();
        }
        return null;
      }
      return !!control.parent &&
        !!control.parent.value &&
        control.value === (control.parent?.controls as any)[matchTo].value
        ? null
        : { matching: true };
    };
  }

  onClose(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {}
}

export class CustomValidators {
  constructor() {}

  static mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors['mustMatch']) {
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
      return null;
    };
  }
}
