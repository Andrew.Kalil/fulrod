import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs';
import { Image } from 'src/app/core/models/image.model';
import { Product } from 'src/app/core/models/product.model';
import { ProductState } from 'src/app/state/products/products.reducers';
import { productSelector } from 'src/app/state/products/products.selectors';

export interface ProductModalData {
  product: Product;
  isCreate: boolean;
  addProduct?: (product: Product, image?: Image) => {};
  updateProduct?: (id: string, product: Product) => {};
  addProductImage: (id: string, file: File) => {};
}

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss'],
})
export class ProductModalComponent implements OnInit, OnDestroy {
  productForm!: FormGroup;
  productSubscription: any;
  singleProduct!: Product;
  isLoading!: boolean;
  newImage: BehaviorSubject<Image> = new BehaviorSubject<Image>({} as Image);

  constructor(
    public dialogRef: MatDialogRef<ProductModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductModalData,
    private productStore: Store<ProductState>
  ) {
    this.productSubscription = this.productStore
      .select(productSelector)
      .subscribe((value) => {
        this.singleProduct = value.currentProduct;
        this.isLoading = value.loading;
        !this.data.isCreate && this.setFormValues(value.currentProduct);
      });
  }

  ngOnInit(): void {
    this.initForm();
    // this.productForm.valueChanges.subscribe((value) => console.log(value));
  }

  // Inital form values
  initForm() {
    this.productForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      image: new FormControl({} as Image, [Validators.required]),
      category: new FormControl('', [Validators.required]),
      price: new FormControl(0, [Validators.required]),
      promotion: new FormControl('', [Validators.required]),
      discount: new FormControl(0.0, []),
      quantity: new FormControl(0, [Validators.required]),
    });

    if (!this.data.isCreate) {
      this.setFormValues(this.singleProduct);
    }
  }

  // Set form values if edit
  setFormValues(product: Product) {
    if (product) {
      const {
        name,
        description,
        image,
        category,
        price,
        promotion,
        discount,
        quantity,
      } = product;
      this.productForm.get('name')?.setValue(name);
      this.productForm.get('description')?.setValue(description);
      this.productForm.get('image')?.setValue(image ? image : ({} as Image));
      this.productForm.get('category')?.setValue(this.handleCategory(category));
      this.productForm.get('price')?.setValue(price);
      this.productForm
        .get('promotion')
        ?.setValue(this.hanglePromotion(promotion));
      this.productForm.get('discount')?.setValue(discount);
      this.productForm.get('quantity')?.setValue(quantity);
    }
  }

  // Submit form function
  onSubmit(event: Event): void {
    event.preventDefault();
    this.productForm
      .get('category')
      ?.setValue(this.handleCategorySubmit(this.productForm.value.category));
    this.productForm
      .get('promotion')
      ?.setValue(this.handlePromotionSubmit(this.productForm.value.promotion));
    if (this.productForm.value.promotion === false) {
      this.productForm.get('discount')?.setValue(0.0);
    }
    if (this.data.isCreate && this.data.addProduct) {
      this.data.addProduct(this.productForm.value, this.newImage.value);
    } else if (
      !this.data.isCreate &&
      this.data.updateProduct &&
      this.singleProduct.id
    ) {
      this.data.updateProduct(this.singleProduct.id, this.productForm.value);
    }
    this.dialogRef.close();
  }

  // CATEGORY CONVERT HANDLER
  handleCategory(category: number): string {
    switch (category) {
      case 1:
        return 'Bolsos';
      case 2:
        return 'Vestidos de baño';
      default:
        return 'NINGUNO';
    }
  }
  handleCategorySubmit(category: string): number {
    switch (category) {
      case 'Bolsos':
        return 1;
      case 'Vestidos de baño':
        return 2;
      default:
        return 0;
    }
  }
  // PROMOTION CONVERT HANDLER
  hanglePromotion(promotion: boolean): string {
    switch (promotion) {
      case true:
        return 'Si';
      case false:
        return 'No';
      default:
        return 'No';
    }
  }
  handlePromotionSubmit(promotion: string): boolean {
    switch (promotion) {
      case 'Si':
        return true;
      case 'No':
        return false;
      default:
        return false;
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {}
}
