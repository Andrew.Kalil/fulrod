import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/core/models/product.model';
import { User } from 'src/app/core/models/user.model';

export interface ModalData {
  product?: Product;
  user?: User;
  deleteProduct?: (id: string) => {};
  deleteUser?: (id: string) => {};
}

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  handleDelete(): void {
    if (this.data.product && this.data.product.id && this.data.deleteProduct) {
      this.data.deleteProduct(this.data.product.id);
    } else if (this.data.user && this.data.deleteUser) {
      this.data.deleteUser(this.data.user.id);
    }
    this.dialogRef.close();
  }
}
