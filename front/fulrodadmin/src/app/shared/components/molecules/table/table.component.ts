import { Component, Input, OnInit } from '@angular/core';
import { Column } from 'src/app/core/models/column.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() displayedColumns!: Column[];
  @Input() isLoading!: boolean;
  @Input() dataSource!: any[];
  @Input() singleItem!: any;
  @Input() getOneItem!: (id: string) => void;

  columnsModified!: string[];

  constructor() {}

  ngOnInit(): void {
    this.columnsModified = this.displayedColumns.map((value) => value.property);
  }
}
