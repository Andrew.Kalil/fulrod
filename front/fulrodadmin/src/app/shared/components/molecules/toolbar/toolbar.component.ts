import { Component, Input, OnInit } from '@angular/core';
import { FilterOptions } from 'src/app/core/models/filterOptions.model';
import { OrderOptions } from 'src/app/core/models/orderOptions.model';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @Input() openCreateModal!: () => void;
  @Input() openEditModal!: () => void;
  @Input() deleteModal!: () => void;
  @Input() filterOptions!: FilterOptions[];
  @Input() orderOptions!: OrderOptions[];

  constructor() {}

  ngOnInit(): void {}
}
