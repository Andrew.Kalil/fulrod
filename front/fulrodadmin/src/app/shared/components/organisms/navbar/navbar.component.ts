import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavLink } from 'src/app/core/models/navLinks.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  navLinks: NavLink[] = [
    { name: 'inicio', route: '/home' },
    { name: 'productos', route: '/products' },
    { name: 'usuarios', route: '/users' },
  ];
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToLink(link: string): void {
    this.router.navigateByUrl(link);
  }
}
