import { Component, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Image } from 'src/app/core/models/image.model';
import { Product } from 'src/app/core/models/product.model';
import { ProductModalData } from '../../modals/product-modal/product-modal.component';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent implements OnInit {
  @Input() image!: Image;
  @Input() data!: ProductModalData;
  @Input() addProductImage!: (id: string, image: File) => {};
  @Input() newImage!: BehaviorSubject<Image>;
  imageToUpload!: Image;

  constructor() {}

  ngOnInit(): void {
    this.loadImage();
  }

  loadImage(): void {
    if (this.image) {
      this.imageToUpload = {
        imageUrl: this.image.imageUrl,
      };
    }
  }

  selectFile(event: any): void {
    this.imageToUpload.file = event.target.files[0];
    if (this.imageToUpload.file) {
      const reader = new FileReader();

      reader.onload = (e: any) =>
        (this.imageToUpload.imageUrl = e.target.result);

      reader.readAsDataURL(this.imageToUpload.file);
    }
    if (
      !this.data.isCreate &&
      this.data.product.id &&
      this.addProductImage &&
      this.imageToUpload.file
    ) {
      this.addProductImage(this.data.product.id, this.imageToUpload.file);
    } else {
      this.imageToUpload.file && this.newImage.next(this.imageToUpload);
    }
  }

  removeImage(): void {
    this.imageToUpload = {} as Image;
  }
}
