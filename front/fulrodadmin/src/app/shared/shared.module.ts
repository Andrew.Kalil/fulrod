import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/organisms/navbar/navbar.component';
import { LogoComponent } from './components/atoms/logo/logo.component';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { TableComponent } from './components/molecules/table/table.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { DataHandlerPipe } from './pipes/table/data-handler.pipe';
import { CurrencyPipe } from '@angular/common';
import { ProductModalComponent } from './components/modals/product-modal/product-modal.component';
import { UserModalComponent } from './components/modals/user-modal/user-modal.component';
import { ConfirmationComponent } from './components/modals/confirmation/confirmation.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { AddButtonComponent } from './components/atoms/add-button/add-button.component';
import { ToolbarComponent } from './components/molecules/toolbar/toolbar.component';
import { EditButtonComponent } from './components/atoms/edit-button/edit-button.component';
import { DeleteButtonComponent } from './components/atoms/delete-button/delete-button.component';
import { TitlePipe } from './pipes/modal/title.pipe';
import { IdPipe } from './pipes/modal/id.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ImageUploadComponent } from './components/atoms/image-upload/image-upload.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MenuButtonComponent } from './components/atoms/menu-button/menu-button.component';
import { MatMenuModule } from '@angular/material/menu';
import { SortButtonComponent } from './components/atoms/sort-button/sort-button.component';

@NgModule({
  declarations: [
    NavbarComponent,
    LogoComponent,
    TableComponent,
    DataHandlerPipe,
    ProductModalComponent,
    UserModalComponent,
    ConfirmationComponent,
    AddButtonComponent,
    ToolbarComponent,
    EditButtonComponent,
    DeleteButtonComponent,
    TitlePipe,
    IdPipe,
    ImageUploadComponent,
    MenuButtonComponent,
    SortButtonComponent,
  ],
  exports: [
    NavbarComponent,
    LogoComponent,
    TableComponent,
    DataHandlerPipe,
    ProductModalComponent,
    UserModalComponent,
    ConfirmationComponent,
    AddButtonComponent,
    ToolbarComponent,
    EditButtonComponent,
    DeleteButtonComponent,
    TitlePipe,
    IdPipe,
    ImageUploadComponent,
    MenuButtonComponent,
    SortButtonComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
  ],
  providers: [CurrencyPipe],
})
export class SharedModule {}
