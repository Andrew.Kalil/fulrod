import { Pipe, PipeTransform } from '@angular/core';
import { ModalData } from '../../components/modals/confirmation/confirmation.component';

@Pipe({
  name: 'id',
})
export class IdPipe implements PipeTransform {
  transform(value: ModalData, ...args: unknown[]): string {
    if (value.user) {
      return value.user.id;
    }
    if (value.product && value.product.id) {
      return value.product.id;
    }
    return '';
  }
}
