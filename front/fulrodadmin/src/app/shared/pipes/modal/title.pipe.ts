import { Pipe, PipeTransform } from '@angular/core';
import { ModalData } from '../../components/modals/confirmation/confirmation.component';

@Pipe({
  name: 'title',
})
export class TitlePipe implements PipeTransform {
  transform(value: ModalData, ...args: unknown[]): string {
    if (value.user) {
      return value.user.username;
    }
    if (value.product) {
      return value.product.name;
    }
    return '';
  }
}
