import { CurrencyPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataHandler',
})
export class DataHandlerPipe implements PipeTransform {
  constructor(private currencyPipe: CurrencyPipe) {}

  transform(value: string | number, ...args: unknown[]): unknown {
    if (args.length < 1) {
      return value;
    }
    switch (args[0]) {
      case 'price':
        return this.currencyPipe.transform(value);
      case 'category':
        return value === 1
          ? 'Bolsos'
          : value === 2
          ? 'Vestidos de baños'
          : 'NINGUNA';
      default:
        return value;
    }
  }
}
