import { Column } from '../core/models/column.model';

export const usersDisplayedColumns: Column[] = [
  { property: 'username', displayName: 'usuario' },
  { property: 'givenName', displayName: 'nombre' },
  { property: 'surname', displayName: 'apellido' },
  { property: 'emailAddress', displayName: 'correo' },
  { property: 'role', displayName: 'rol' },
];

export const productsDisplayColumns: Column[] = [
  { property: 'name', displayName: 'nombre' },
  { property: 'category', displayName: 'categoria' },
  { property: 'price', displayName: 'precio' },
  { property: 'discount', displayName: 'descuento' },
  { property: 'image', displayName: 'imagen' },
];
