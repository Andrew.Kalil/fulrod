import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from './users.reducers';

const usersFeatureSelector = createFeatureSelector<UserState>('users');

export const userSelector = createSelector(
  usersFeatureSelector,
  (state: UserState) => state
);
