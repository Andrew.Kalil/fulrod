import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of } from 'rxjs';
import { UsersService } from 'src/app/core/services/users/users.service';
import {
  addUser,
  addUserFailure,
  addUserSuccess,
  deleteUser,
  deleteUserFailure,
  deleteUserSuccess,
  getUser,
  getUserFailure,
  getUsers,
  getUsersFailure,
  getUsersSuccess,
  getUserSuccess,
  updateUser,
  updateUserFailure,
  updateUserSuccess,
} from './users.actions';

@Injectable()
export class UsersEffects {
  constructor(private usersServices: UsersService, private actions: Actions) {}

  public getUserEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(getUser),
      mergeMap((action) => {
        return this.usersServices.getBy(action.userId).pipe(
          map((user) => getUserSuccess(user)),
          catchError(() => of(getUserFailure()))
        );
      })
    );
  });

  public getUsersEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(getUsers),
      mergeMap(() => {
        return this.usersServices.getAll().pipe(
          map((users) => getUsersSuccess(users)),
          catchError(() => of(getUsersFailure()))
        );
      })
    );
  });

  public addUserEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(addUser),
      mergeMap((action) => {
        return this.usersServices.create(action.user).pipe(
          map(() => addUserSuccess()),
          map(() => getUsers()),
          catchError(() => of(addUserFailure()))
        );
      })
    );
  });

  public updateUserEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(updateUser),
      mergeMap((action) => {
        return this.usersServices.update(action.user, action.userId).pipe(
          map(() => addUserSuccess()),
          map(() => getUsers()),
          catchError(() => of(updateUserFailure()))
        );
      })
    );
  });

  public deleteUserEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(deleteUser),
      mergeMap((action) => {
        return this.usersServices.delete(action.userId).pipe(
          map(() => deleteUserSuccess()),
          map(() => getUsers()),
          catchError(() => of(deleteUserFailure()))
        );
      })
    );
  });
}
