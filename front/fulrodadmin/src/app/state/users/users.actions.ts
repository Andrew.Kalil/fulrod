import { createAction } from '@ngrx/store';
import { User } from 'src/app/core/models/user.model';

// USER
export const getUser = createAction('[GET USER]', (userId: string) => ({
  userId,
}));
export const getUserSuccess = createAction(
  '[GET_USER] SUCCESS',
  (user: User) => ({ user })
);
export const getUserFailure = createAction('[GET_USER] FAILURE');
export const resetUser = createAction('[RESET USER]');

// USERS
export const getUsers = createAction('[GET USERS]');
export const getUsersSuccess = createAction(
  '[GET_USERS] SUCCESS',
  (users: User[]) => ({ users })
);
export const getUsersFailure = createAction('[GET_USERS] FAILURE');
export const resetUsers = createAction('[RESET USERS]');

// ADD USER
export const addUser = createAction('[ADD USER]', (user: User) => ({
  user,
}));
export const addUserSuccess = createAction('[ADD_USER] SUCCESS');
export const addUserFailure = createAction('[ADD_USER] FAILURE');

// UPDATE USER
export const updateUser = createAction(
  '[UPDATE USER]',
  (userId: string, user: User) => ({ userId, user })
);
export const updateUserSuccess = createAction('[UPDATE_USER] SUCCESS');
export const updateUserFailure = createAction('[UPDATE_USER] FAILURE');

// DELETE USER
export const deleteUser = createAction('[DELETE USER]', (userId: string) => ({
  userId,
}));
export const deleteUserSuccess = createAction('[DELETE_USER] SUCCESS');
export const deleteUserFailure = createAction('[DELETE_USER] FAILURE');

// MODAL
// export const openUserModal = createAction(
//   '[OPEN USER MODAL]',
//   (isCreate: boolean) => ({
//     isCreate,
//   })
// );
// export const closeUserModal = createAction('[CLOSE USER MODAL]');
