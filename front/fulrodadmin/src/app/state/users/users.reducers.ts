import { createReducer, on } from '@ngrx/store';
import { User } from 'src/app/core/models/user.model';
import {
  addUser,
  addUserFailure,
  addUserSuccess,
  deleteUser,
  deleteUserFailure,
  deleteUserSuccess,
  getUser,
  getUserFailure,
  getUsers,
  getUsersFailure,
  getUsersSuccess,
  getUserSuccess,
  resetUser,
  resetUsers,
  updateUser,
  updateUserFailure,
  updateUserSuccess,
} from './users.actions';

export interface UserState {
  users: User[];
  currentUser: User;
  loading: boolean;
  error: string;
  isOpen: boolean;
  isCreate: boolean;
}

export const initialState: UserState = {
  users: [],
  currentUser: {} as User,
  loading: false,
  error: '',
  isOpen: false,
  isCreate: true,
};

export const userReducer = createReducer(
  initialState,

  // GET USER
  on(getUser, (state) => {
    return { ...state, loading: true };
  }),
  on(getUserSuccess, (state, { user }) => {
    return { ...state, loading: false, currentUser: user };
  }),
  on(getUserFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),
  on(resetUser, (state) => {
    return { ...state, currentUser: {} as User };
  }),

  // GET USERS
  on(getUsers, (state) => {
    return { ...state, loading: true };
  }),
  on(getUsersSuccess, (state, { users }) => {
    return { ...state, loading: false, users: users };
  }),
  on(getUsersFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),
  on(resetUsers, (state) => {
    return { ...state, users: [] };
  }),

  // ADD USER
  on(addUser, (state) => {
    return { ...state, loading: true };
  }),
  on(addUserSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(addUserFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // UPDATE USER
  on(updateUser, (state) => {
    return { ...state, loading: true };
  }),
  on(updateUserSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(updateUserFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // DELETE USER
  on(deleteUser, (state) => {
    return { ...state, loading: true };
  }),
  on(deleteUserSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(deleteUserFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  })

  // MODAL
  // on(openUserModal, (state, { isCreate }) => {
  //   return { ...state, isOpen: true, isCreate };
  // }),
  // on(closeUserModal, (state) => {
  //   return { ...state, isOpen: false, isCreate: false };
  // })
);
