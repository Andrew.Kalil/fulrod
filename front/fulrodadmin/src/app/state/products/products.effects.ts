import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, mergeMap, of } from 'rxjs';
import { Queries } from 'src/app/core/models/filter';
import { Product } from 'src/app/core/models/product.model';
import { ProductsService } from 'src/app/core/services/products/products.service';
import {
  addProduct,
  addProductFailure,
  addProductImage,
  addProductImageFailure,
  addProductImageSuccess,
  addProductSuccess,
  deleteProduct,
  deleteProductFailure,
  deleteProductSuccess,
  getProduct,
  getProductFailure,
  getProducts,
  getProductsFailure,
  getProductsSuccess,
  getProductSuccess,
  queryProducts,
  queryProductsFailure,
  queryProductsSuccess,
  updateProduct,
  updateProductFailure,
  updateProductSuccess,
} from './products.actions';
import { ProductState } from './products.reducers';
import { productSelector } from './products.selectors';

@Injectable()
export class ProductsEffects {
  queries: Queries = {};
  constructor(
    private prodructServices: ProductsService,
    private actions: Actions,
    private productStore: Store<ProductState>
  ) {
    this.productStore.select(productSelector).subscribe((value) => {
      this.queries = value.appliedQueries;
    });
  }

  public getProductEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(getProduct),
      mergeMap((action) => {
        return this.prodructServices.getBy(action.productId).pipe(
          map((product) => getProductSuccess(product)),
          catchError(() => of(getProductFailure()))
        );
      })
    );
  });

  public getProductsEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(getProducts),
      mergeMap(() => {
        return this.prodructServices.getAll(this.queries).pipe(
          map((products) => getProductsSuccess(products)),
          catchError(() => of(getProductsFailure()))
        );
      })
    );
  });

  public addProductEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(addProduct),
      mergeMap((action) => {
        return this.prodructServices.create(action.product, action.image).pipe(
          map((product: Product) => {
            if (product.id && action.image) {
              let formData = new FormData();
              formData.append('image', action.image, action.image.name);
              const addImage = this.prodructServices.createImage(
                product.id,
                formData
              );
              addImage.subscribe((value) =>
                this.productStore.dispatch(getProducts())
              );
            }
          }),
          map(() => addProductSuccess()),
          map(() => getProducts()),
          catchError(() => of(addProductFailure()))
        );
      })
    );
  });

  public addProductImageEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(addProductImage),
      mergeMap((action) => {
        let formData = new FormData();
        formData.append('image', action.file, action.file.name);
        return this.prodructServices.createImage(action.id, formData).pipe(
          map(() => addProductImageSuccess()),
          map(() => getProduct(action.id)),
          catchError(() => of(addProductImageFailure()))
        );
      })
    );
  });

  public updateProductEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(updateProduct),
      mergeMap((action) => {
        return this.prodructServices
          .update(action.productId, action.product)
          .pipe(
            map(() => addProductSuccess()),
            map(() => getProducts()),
            catchError(() => of(updateProductFailure()))
          );
      })
    );
  });

  public deleteProductEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(deleteProduct),
      mergeMap((action) => {
        return this.prodructServices.delete(action.productId).pipe(
          map(() => deleteProductSuccess()),
          map(() => getProducts()),
          catchError(() => of(deleteProductFailure()))
        );
      })
    );
  });

  public queryProductsEffect = createEffect(() => {
    return this.actions.pipe(
      ofType(queryProducts),
      mergeMap((action) => {
        return this.prodructServices.getAll(action.queries).pipe(
          map((products) => queryProductsSuccess(products)),
          catchError(() => of(queryProductsFailure()))
        );
      })
    );
  });
}
