import { createReducer, on } from '@ngrx/store';
import { Queries } from 'src/app/core/models/filter';
import { Product } from 'src/app/core/models/product.model';
import {
  addProduct,
  addProductFailure,
  addProductImage,
  addProductImageFailure,
  addProductImageSuccess,
  addProductSuccess,
  deleteProduct,
  deleteProductFailure,
  deleteProductSuccess,
  getProduct,
  getProductFailure,
  getProducts,
  getProductsFailure,
  getProductsSuccess,
  getProductSuccess,
  queryProducts,
  queryProductsFailure,
  queryProductsSuccess,
  resetProduct,
  resetProducts,
  updateProduct,
  updateProductFailure,
  updateProductSuccess,
} from './products.actions';

export interface ProductState {
  products: Product[];
  currentProduct: Product;
  loading: boolean;
  error: string;
  isOpen: boolean;
  isCreate: boolean;
  appliedQueries: Queries;
}

export const initialState: ProductState = {
  products: [],
  currentProduct: {} as Product,
  loading: false,
  error: '',
  isOpen: false,
  isCreate: false,
  appliedQueries: {},
};

export const productReducer = createReducer(
  initialState,

  // GET PRODUCT
  on(getProduct, (state) => {
    return { ...state, loading: true };
  }),
  on(getProductSuccess, (state, { product }) => {
    return { ...state, loading: false, currentProduct: product };
  }),
  on(getProductFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),
  on(resetProduct, (state) => {
    return { ...state, currentProduct: {} as Product };
  }),

  // GET PRODUCTS
  on(getProducts, (state) => {
    return { ...state, loading: true };
  }),
  on(getProductsSuccess, (state, { products }) => {
    return { ...state, loading: false, products };
  }),
  on(getProductsFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),
  on(resetProducts, (state) => {
    return { ...state, products: [] };
  }),

  // ADD PRODUCT
  on(addProduct, (state) => {
    return { ...state, loading: true };
  }),
  on(addProductSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(addProductFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // ADD PRODUCT IMAGE
  on(addProductImage, (state) => {
    return { ...state, loading: true };
  }),
  on(addProductImageSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(addProductImageFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // UPDATE PRODUCT
  on(updateProduct, (state) => {
    return { ...state, loading: true };
  }),
  on(updateProductSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(updateProductFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // DELETE PRODUCT
  on(deleteProduct, (state) => {
    return { ...state, loading: true };
  }),
  on(deleteProductSuccess, (state) => {
    return { ...state, loading: false };
  }),
  on(deleteProductFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  }),

  // FILTER BY PROPERTY
  on(queryProducts, (state, { queries }) => {
    return { ...state, loading: true, appliedQueries: queries };
  }),
  on(queryProductsSuccess, (state, { products }) => {
    return { ...state, loading: false, products };
  }),
  on(queryProductsFailure, (state) => {
    return { ...state, loading: false, error: 'Hubo un error :(' };
  })
);
