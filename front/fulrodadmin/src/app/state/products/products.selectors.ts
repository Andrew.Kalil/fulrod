import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProductState } from './products.reducers';

const productFeatureSelector = createFeatureSelector<ProductState>('products');

export const productSelector = createSelector(
  productFeatureSelector,
  (state: ProductState) => state
);
