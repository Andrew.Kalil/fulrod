import { createAction } from '@ngrx/store';
import { Queries } from 'src/app/core/models/filter';
import { Image } from 'src/app/core/models/image.model';
import { Product } from 'src/app/core/models/product.model';

// PRODUCT
export const getProduct = createAction(
  '[GET PRODUCT]',
  (productId: string) => ({ productId })
);
export const getProductSuccess = createAction(
  '[GET_PRODUCT] SUCCESS',
  (product: Product) => ({ product })
);
export const getProductFailure = createAction('[GET_PRODUCT] FAILURE');
export const resetProduct = createAction('[RESET PRODUCT]');

// PRODUCTS
export const getProducts = createAction('[GET PRODUCTS]');
export const getProductsSuccess = createAction(
  '[GET_PRODUCTS] SUCCESS',
  (products: Product[]) => ({ products })
);
export const getProductsFailure = createAction('[GET_PRODUCTS] FAILURE');
export const resetProducts = createAction('[RESET PRODUCTS]');

// ADD PRODUCT
export const addProduct = createAction(
  '[ADD PRODUCT]',
  (product: Product, image?: File) => ({
    product,
    image,
  })
);
export const addProductSuccess = createAction('[ADD_PRODUCT] SUCCESS');
export const addProductFailure = createAction('[ADD_PRODUCT] FAILURE');

// ADD PRODUCT IMAGE
export const addProductImage = createAction(
  '[ADD_PRODUCT_IMAGE]',
  (id: string, file: File) => ({ id, file })
);
export const addProductImageSuccess = createAction(
  '[ADD_PRODUCT_IMAGE] SUCCESS'
);
export const addProductImageFailure = createAction(
  '[ADD_PRODUCT_IMAGE] FAILURE'
);

// UPDATE PRODUCT
export const updateProduct = createAction(
  '[UPDATE PRODUCT]',
  (productId: string, product: Product) => ({ productId, product })
);
export const updateProductSuccess = createAction('[UPDATE_PRODUCT] SUCCESS');
export const updateProductFailure = createAction('[UPDATE_PRODUCT] FAILURE');

// DELETE PRODUCT
export const deleteProduct = createAction(
  '[DELETE PRODUCT]',
  (productId: string) => ({ productId })
);
export const deleteProductSuccess = createAction('[DELETE_PRODUCT] SUCCESS');
export const deleteProductFailure = createAction('[DELETE_PRODUCT] FAILURE');

// QUERY BY PROPERTY
export const queryProducts = createAction(
  '[QUERY PRODUCTS]',
  (queries: Queries) => ({ queries })
);
export const queryProductsSuccess = createAction(
  '[QUERY_PRODUCTS] SUCCESS',
  (products: Product[]) => ({ products })
);
export const queryProductsFailure = createAction('[QUERY_PRODUCTS] FAILURE');
