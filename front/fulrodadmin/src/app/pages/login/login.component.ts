import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formGroup!: FormGroup;
  isLogged: boolean = false;
  constructor(private authService: AuthService, private router: Router) {
    this.authService.isLogged.subscribe((value) => (this.isLogged = value));
  }

  ngOnInit(): void {
    this.initForm();
    this.authService.user.subscribe((value) => console.log(value));
  }

  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  login() {
    if (this.formGroup.valid) {
      this.authService.login(this.formGroup.value).subscribe((value) => {
        if (value.response === 'Success') {
          this.authService.setUser(value.userId).subscribe(() => {
            this.router.navigate(['home']);
          });
          // console.log(value.token);
        } else {
          // Do notification (Login fail)
          alert('Login Fail');
        }
      });
    }
  }
}
