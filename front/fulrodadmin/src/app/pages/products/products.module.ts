import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsComponent } from './products.component';
import { CoreModule } from 'src/app/core/core.module';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';

const routes: Routes = [
  { path: 'products', component: ProductsComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [ProductsComponent],
  exports: [ProductsComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule,
    MatTableModule,
    MatDialogModule,
  ],
})
export class ProductsModule {}
