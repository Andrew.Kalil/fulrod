import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Column } from 'src/app/core/models/column.model';
import { Queries } from 'src/app/core/models/filter';
import { FilterOptions } from 'src/app/core/models/filterOptions.model';
import { Image } from 'src/app/core/models/image.model';
import { OrderOptions } from 'src/app/core/models/orderOptions.model';
import { Product } from 'src/app/core/models/product.model';
import { ConfirmationComponent } from 'src/app/shared/components/modals/confirmation/confirmation.component';
import { ProductModalComponent } from 'src/app/shared/components/modals/product-modal/product-modal.component';
import { productsDisplayColumns } from 'src/app/shared/constants';
import {
  addProduct,
  addProductImage,
  deleteProduct,
  getProduct,
  getProducts,
  queryProducts,
  resetProduct,
  updateProduct,
} from 'src/app/state/products/products.actions';
import { ProductState } from 'src/app/state/products/products.reducers';
import { productSelector } from 'src/app/state/products/products.selectors';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  singleProduct: Product = {} as Product;
  isLoading: boolean = true;
  displayedColumns: Column[] = [...productsDisplayColumns];
  queries: Queries = {};
  filterOptions: FilterOptions[] = [
    {
      option: 'Ninguno',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, property: '' })
        );
      },
    },
    {
      option: 'Bolsos',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, property: 'category=1' })
        );
      },
    },
    {
      option: 'Vestidos de baño',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, property: 'category=2' })
        );
      },
    },
  ];
  orderOptions: OrderOptions[] = [
    {
      option: 'Ninguno',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, sort: '' })
        );
      },
    },
    {
      option: 'Mayor a menor Precio',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, sort: 'price=DSC' })
        );
      },
    },
    {
      option: 'Menor a mayor Precio',
      callback: () => {
        this.productStore.dispatch(
          queryProducts({ ...this.queries, sort: 'price=ASC' })
        );
      },
    },
  ];

  //subscriptions
  productSubscription: any;

  constructor(
    private productStore: Store<ProductState>,
    public dialog: MatDialog
  ) {
    this.productSubscription = this.productStore
      .select(productSelector)
      .subscribe((value) => {
        this.singleProduct = value.currentProduct;
        this.isLoading = value.loading;
        this.products = value.products;
        this.queries = value.appliedQueries;
      });
  }

  ngOnInit(): void {
    this.getProducts();
  }

  // CRUD OPERATIONS
  getProducts(): void {
    this.productStore.dispatch(getProducts());
  }
  getSingleProduct = (id: string): void => {
    this.productStore.dispatch(getProduct(id));
  };
  updateProduct = (id: string, product: Product): void => {
    this.productStore.dispatch(updateProduct(id, product));
  };
  addProduct = (product: Product, image?: Image): void => {
    if (image) {
      this.productStore.dispatch(addProduct(product, image.file));
    } else {
      this.productStore.dispatch(addProduct(product));
    }
  };
  addProductImage = (id: string, image: File): void => {
    this.productStore.dispatch(addProductImage(id, image));
  };
  deleteProduct = (id: string): void => {
    this.productStore.dispatch(deleteProduct(id));
  };

  // MODAL FUNCTIONS
  openCreateModal = (): void => {
    this.dialog.open(ProductModalComponent, {
      width: '500px',
      height: '600px',
      data: {
        isCreate: true,
        addProduct: this.addProduct,
      },
    });
  };
  openEditModal = (): void => {
    if (this.singleProduct.id) {
      const openModal = this.dialog.open(ProductModalComponent, {
        width: '500px',
        height: '600px',
        data: {
          product: this.singleProduct,
          isCreate: false,
          updateProduct: this.updateProduct,
          addProductImage: this.addProductImage,
        },
      });
      openModal
        .afterClosed()
        .subscribe(() => this.productStore.dispatch(resetProduct()));
    }
  };
  deleteModal = (): void => {
    if (this.singleProduct.id) {
      const confirmModal = this.dialog.open(ConfirmationComponent, {
        width: '500px',
        data: {
          product: this.singleProduct,
          deleteProduct: this.deleteProduct,
          addProductImage: this.addProductImage,
        },
      });
      confirmModal
        .afterClosed()
        .subscribe(() => this.productStore.dispatch(resetProduct()));
    }
  };

  ngOnDestroy(): void {
    this.productSubscription.unsubscribe();
  }
}
