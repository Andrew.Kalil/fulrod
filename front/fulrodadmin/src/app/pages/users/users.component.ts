import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Column } from 'src/app/core/models/column.model';
import { User } from 'src/app/core/models/user.model';
import { ConfirmationComponent } from 'src/app/shared/components/modals/confirmation/confirmation.component';
import { UserModalComponent } from 'src/app/shared/components/modals/user-modal/user-modal.component';
import { usersDisplayedColumns } from 'src/app/shared/constants';
import {
  addUser,
  deleteUser,
  getUser,
  getUsers,
  resetUser,
  updateUser,
} from 'src/app/state/users/users.actions';
import { UserState } from 'src/app/state/users/users.reducers';
import { userSelector } from 'src/app/state/users/users.selectors';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  users: User[] = [];
  singleUser: User = {} as User;
  isLoading: boolean = true;
  displayedColumns: Column[] = [...usersDisplayedColumns];

  //subscriptions
  userSubscription: any;

  constructor(private userStore: Store<UserState>, public dialog: MatDialog) {
    this.userSubscription = this.userStore
      .select(userSelector)
      .subscribe((value) => {
        this.singleUser = value.currentUser;
        this.isLoading = value.loading;
        this.users = value.users;
      });
  }

  ngOnInit(): void {
    this.getUsers();
  }

  // CRUD OPERATIONS
  getUsers(): void {
    this.userStore.dispatch(getUsers());
  }
  getSingleUser = (id: string): void => {
    this.userStore.dispatch(getUser(id));
  };
  updateUser = (id: string, user: User): void => {
    this.userStore.dispatch(updateUser(id, user));
  };
  addUser = (user: User): void => {
    this.userStore.dispatch(addUser(user));
  };
  deleteUser = (id: string): void => {
    this.userStore.dispatch(deleteUser(id));
  };

  // MODAL FUNCTIONS
  openCreateModal = (): void => {
    const openModal = this.dialog.open(UserModalComponent, {
      width: '500px',
      data: {
        isCreate: true,
        addUser: this.addUser,
      },
    });
    openModal
      .afterClosed()
      .subscribe(() => this.userStore.dispatch(resetUser()));
  };
  openEditModal = (): void => {
    if (this.singleUser.id) {
      const openModal = this.dialog.open(UserModalComponent, {
        width: '500px',
        data: {
          user: this.singleUser,
          isCreate: false,
          updateUser: this.updateUser,
        },
      });
      openModal
        .afterClosed()
        .subscribe(() => this.userStore.dispatch(resetUser()));
    }
  };
  deleteModal = (): void => {
    if (this.singleUser.id) {
      const confirmModal = this.dialog.open(ConfirmationComponent, {
        width: '500px',
        data: {
          user: this.singleUser,
          deleteUser: this.deleteUser,
        },
      });
      confirmModal
        .afterClosed()
        .subscribe(() => this.userStore.dispatch(resetUser()));
    }
  };

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
