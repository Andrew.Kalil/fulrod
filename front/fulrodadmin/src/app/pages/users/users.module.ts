import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { CoreModule } from 'src/app/core/core.module';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { MatDialogModule } from '@angular/material/dialog';

const routes: Routes = [
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [UsersComponent],
  exports: [UsersComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule,
    MatDialogModule,
  ],
})
export class UsersModule {}
